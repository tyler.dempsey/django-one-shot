from django.contrib import admin
from .models import TodoList
from .models import TodoItem

admin.site.register(TodoList)
admin.site.register(TodoItem)
